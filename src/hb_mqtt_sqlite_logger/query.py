#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The programm can be used query the MQTT-SQLite-Logger database.

Refer to README.md for more details.

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL V. 1.2. Refer to license files on disc.
"""

from argparse import ArgumentParser
import getpass
import datetime
import json
import os
import paho.mqtt.publish as publish
import matplotlib.pyplot as plt
import paho.mqtt.client as mqtt  # import the client
import time


def on_message(client, userdata, message):
    """Handle received MQTT topic."""
    try:
        payload = message.payload.decode("utf-8")
        if message.topic.find('sql_status') >= 0:
            print("SQL Status received ", payload)
            pass
        else:
            # print("SQL Results received ", payload)
            sql_query_result = json.loads(payload)
            # print('sql_query_result:', sql_query_result['sql_query_result'])
            data_times = []
            data_values = []
            ii = 0
            for row in sql_query_result['sql_query_result']:
                data_times.append(datetime.datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S'))
                data_values.append(row[1])
                ii += 1
            # print('ii=', ii, 'len(t)=', len(data_times), 'len(v)=', len(data_values))
            try:
                plt.plot(data_times, data_values)
                plt.show()
            except Exception as e:
                print('on_message: Cannot plot data. Exception cought:', e)
            pass
        pass
    except Exception as e:
        print('on_message: Exception cought:', e)
        pass


def run_query(sql_query, broker='localhost', logger='mqtt-sqlite-logger', client_id='mqtt-sqlite-query', sql_result_topic='sql_query_result', timeout=10, user=None, pwd=None):
    """
    This function can be used to query SQLite database.

    Python API entry point for query.

    Parameters
    ----------
    sql_query : str
        SQL query.
    broker : str, optional
        MQTT broker node. The default is 'localhost'.
    logger : string, optional
        MQTT topic used by mqtt_sql_logger. The default is 'mqtt-sqlite-logger'.
    client_id : string, optional
        MQTT client_id. The default is 'mqtt-sqlite-query'.
    sql_result_topic : string, optional
        MQTT topic name to publish SQL query result. The default is 'sql_query_result'.
    timeout : float, optional
        Timeout in seconds. The default is 10.

    Returns
    -------
    None.

    """
    try:
        sql_query_command = {"sql_query": sql_query, "sql_result_topic": client_id + '/' + sql_result_topic}
        # print('SQL query command:', sql_query_command)
        # print('')
        print('Creating new instance:', client_id)
        client = mqtt.Client(client_id)  # create new instance
        if user is not None and pwd is not None:
            client.username_pw_set(username=user, password=pwd)
        client.on_message = on_message  # attach function to callback
        print('Connecting to broker:', broker)
        client.connect(broker)  # connect to broker
        client.loop_start()  # start the loop
        print('Subscribing to topic:', logger + "/sql_status")
        client.subscribe(logger + '/sql_status')
        print('Subscribing to topic:', sql_query_command['sql_result_topic'])
        client.subscribe(client_id + '/' + sql_result_topic, qos=2)
        publish.single(logger + '/sql_query', json.dumps(sql_query_command), qos=2, hostname=broker)
        time.sleep(timeout)  # wait
    except Exception as e:
        print('main: Exception cought:', e)
        pass
    finally:
        if not client:
            client.unsubscribe('#')
            client.loop_stop()  # stop the loop
    pass


def cli():
    """Command-line interface for mqtt-sqlite-query."""
    parser = ArgumentParser(
        description='The programm can be used query the MQTT-SQLite-Logger database.',
        epilog='Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH',
        prog='py query.py'
    )
    parser.add_argument('-b', '--broker', dest='broker', default='localhost')
    parser.add_argument('-c', '--client_id', dest='client_id', default='mqtt-sqlite-query')
    parser.add_argument('-l', '--logger', dest='logger', default='mqtt-sqlite-logger')
    parser.add_argument('-q', '--query_filename', dest='query', default='query.json')
    parser.add_argument('-t', '--timeout', dest='timeout', default=1)
    args = parser.parse_args()
    broker = args.broker
    client_id = args.client_id
    logger = args.logger
    query = args.query
    timeout = args.timeout
    with open(query,) as fObj:
        content = json.load(fObj)
        sql_query = content['sql_query']
        # print(sql_query)
        sql_result_topic = content['sql_result_topic']
        # print(sql_result_topic)
    if broker == 'localhost':
        tmp = input('MQTT Broker (' + broker + ')? ')
        if tmp != '':
            broker = tmp
    user = input('MQTT username: ')
    if len(user) == 0:
        user = None
    pwd = getpass.getpass()
    if len(pwd) == 0:
        pwd = None
    run_query(sql_query, broker, logger, client_id, sql_result_topic, timeout, user, pwd)


if __name__ == '__main__':
    print(os.path.basename(__file__), "\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n\
Published under EUPL.\n\
        ")
    cli()
    print("Program stopped.")
