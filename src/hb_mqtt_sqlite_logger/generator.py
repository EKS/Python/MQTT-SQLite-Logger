#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The programm can be used generate the MQTT topic data for testing purpose.

Refer to README.md for more details.

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL V. 1.2. Refer to license files on disc.
"""
from argparse import ArgumentParser
import getpass
import json
import os
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt  # import the client
from random import random
import time


def run_generator(broker='localhost', client_id='mqtt-sqlite-generator', number_of_samples=100, delta_t=1, user=None, pwd=None):
    """
    This function can be used to generate some random value topics (integer and double).

    Python API entry point for generator.

    Parameters
    ----------
    broker : string, optional
        MQTT broker node. The default is 'localhost'.
    client_id : srring, optional
        MQTT client node. The default is 'mqtt-sqlite-generator'.
    number_of_samples : int, optional
        Number of samples to generate. The default is 100.
    delta_t : TYPE, float
        Time inerval for sample generation in seconds. The default is 1.

    Returns
    -------
    None.

    """
    try:
        print('Creating new instance:', client_id)
        client = mqtt.Client(client_id)  # create new instance
        if user is not None and pwd is not None:
            client.username_pw_set(username=user, password=pwd)
        print('Connecting to broker:', broker)
        client.connect(broker)  # connect to broker
        client.loop_start()  # start the loop
        for _ in range(number_of_samples):
            d = random()
            publish.single(client_id + '/double', json.dumps(d), hostname=broker)
            publish.single(client_id + '/integer', json.dumps(int(10 * d)), hostname=broker)
            time.sleep(delta_t)  # wait
    except Exception as e:
        print('Exception cought:', e)
        pass
    finally:
        if not client:
            client.loop_stop()  # stop the loop
    pass


def cli():
    """Command-line interface for mqtt-sqlite-generator."""
    parser = ArgumentParser(
        description='The programm can be used generate the MQTT topic data for testing purpose.',
        epilog='Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH',
        prog='py generator.py'
    )
    parser.add_argument('-b', '--broker', dest='broker', default='localhost')
    parser.add_argument('-c', '--client_id', dest='client_id', default='mqtt-sqlite-generator')
    parser.add_argument('-s', '--samples', dest='samples', default=1000)
    parser.add_argument('-t', '--delta_t', dest='delta_t', default=1)
    args = parser.parse_args()
    broker = args.broker
    client_id = args.client_id
    samples = args.samples
    delta_t = args.delta_t
    print('Generating {0} random data samples, estimated time is {1}s'.format(samples, samples * delta_t))
    if broker == 'localhost':
        tmp = input('MQTT Broker? ' + broker + ')? ')
        if tmp != '':
            broker = tmp
    user = input('MQTT username: ')
    if len(user) == 0:
        user = None
    pwd = getpass.getpass()
    if len(pwd) == 0:
        pwd = None
    run_generator(broker, client_id, samples, delta_t, user, pwd)


if __name__ == '__main__':
    print(os.path.basename(__file__), "\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n\
Published under EUPL.\n\
        ")
    cli()
    print("Program stopped.")
