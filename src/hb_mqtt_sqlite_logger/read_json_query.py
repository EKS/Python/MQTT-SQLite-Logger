# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 14:02:42 2023

@author: brand
"""

import json

print('Using sql query from file.')
with open('D:\\Brand\\Seafile\\Meine Bibliothek\\Python\\hb_mqtt_sqlite_logger\\src\\hb_mqtt_sqlite_logger\\query.json',) as fObj:
    content = json.load(fObj)
    sql_query = content['sql_query']
    print(sql_query)
    sql_result_topic = content['sql_result_topic']
    print(sql_result_topic)
