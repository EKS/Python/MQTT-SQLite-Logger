#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The programm can be used to log MQTT-Topics to SQLite database.

Refer to README.md for more details.

Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL V. 1.1. Refer to license files on disc.
"""

from argparse import ArgumentParser
import getpass
import json
import logging
import os
import paho.mqtt.client as mqtt
import queue
import re
import sqlite3
import sys
import threading
import time
from platformdirs import PlatformDirs


def on_log(client, userdata, level, buf):
    """Handle callback: Log to MQTT message to logger."""
    logging.debug('MQTTlog: ' + buf)
    pass


def on_connect(client, userdata, flags, rc):
    """Handle callback:  MQTT broker is connected."""
    # print('on_connect userdata:', userdata)
    if rc == 0:
        client.connected_flag = True
        logging.info('Connected OK')
        print('Connected OK')
        publish_all(client, userdata)
        subscribe_all(client, userdata)
    else:
        logging.error('Bad connection, returned code =', rc)
    pass


def on_disconnect(client, userdata, flags, rc=0):
    """Handle callback:  MQTT broker is disconnected."""
    # client.connected_flag = False
    logging.warning('Disconnected result code =' + str(rc))
    pass


def on_publish(client, userdata, mid):
    """Handle callback:  Log to logger when topic is published ."""
    # logging.debug('Client published message ID =' + str(mid))
    pass


def on_message(client, userdata, msg):
    """Handle callback:  Handle received MQTT message."""
    # print('on_message userdata:', userdata)
    topic = msg.topic
    m_decode = str(msg.payload.decode("utf-8", "ignore"))
    logging.debug('Message received. Topic: ' + topic + ' Payload: ' + m_decode)
    # print('Message received. Topic: ' + topic + ' Payload: ' + m_decode)
    userdata['q'].put((topic, m_decode))
    pass


def on_subscribe(client, userdata, mid, granted_qos):
    """Handle callback:  Log MQTT topic subscription to logger."""
    logging.debug('Client subscribed message ID =' + str(mid) + 'with qos =' + str(granted_qos))
    pass


def on_unsubscribe(client, userdata, mid):
    """Handle callback:  Log MQTT unsubscribing from topic to logger."""
    logging.debug('Client unsubscribed message ID =' + str(mid))
    pass


def publish_all(client, userdata):
    """Publish all MQTT topics."""
    # client_id
    # print('publish_all userdata:', userdata)
    rc, mid = client.publish(userdata['client_id'], 'connected')
    logging.debug('Publishing: connected returned rc=' + str(rc) + ' mid=' + str(mid))
    # status
    logging.debug('Publishing: topic_status=' + userdata['topic_status'])
    rc, mid = client.publish(userdata['topic_status'], 0)
    logging.debug('Publishing: ' + userdata['topic_status'] + ' returned rc=' + str(rc) + 'mid=' + str(mid))
    # version
    rc, mid = client.publish(userdata['topic_version'], '1.2.0.0')
    logging.debug('Publishing: ' + userdata['topic_version'] + ' returned rc=' + str(rc) + 'mid=' + str(mid))
    # command
    rc, mid = client.publish(userdata['topic_command'], 'Write your command here.')
    logging.debug('Publishing: ' + userdata['topic_command'] + ' returned rc=' + str(rc) + ' mid=' + str(mid))
    # sqlquery
    rc, mid = client.publish(userdata['topic_sql_query'], '{"SQLQuery" : "select * from data where topic_id=0","SQLResponseTopic" : "Test/SQLQueryResponse"}')
    logging.debug('Publishing: ' + userdata['topic_sql_query'] + ' returned rc=' + str(rc) + ' mid=' + str(mid))
    # sqlstatus
    rc, mid = client.publish(userdata['topic_sql_status'], "OK")
    logging.debug('Publishing: ' + userdata['topic_sql_status'] + ' returned rc=' + str(rc) + ' mid=' + str(mid))
    pass


def subscribe_all(client, userdata):
    """Subscribe to all MQTT topics."""
    # print('publish_all userdata:', userdata)
    # command
    rc, mid = client.subscribe(userdata['topic_command'])
    logging.debug('Subscribing to: ' + userdata['topic_command'] + ' returned rc=' + str(rc) + ' mid=' + str(mid))
    # sqlquery
    rc, mid = client.subscribe(userdata['topic_sql_query'], qos=2)
    logging.debug('Subscribing to: ' + userdata['topic_sql_query'] + ' returned rc=' + str(rc) + ' mid=' + str(mid))
    # logger topics
    # print('Subscribe to topics_to_log:', userdata['topics_to_log'])
    for topic in userdata['topics_to_log']:
        logging.debug('Subscribe to ' + topic)
        rc, mid = client.subscribe(topic)
        logging.debug('Subscribing to: ' + topic + ' returned rc=' + str(rc) + ' mid=' + str(mid))
    pass


def unsubscribe_all(client, userdata):
    """Unsubscribe from topics here."""
    # command
    rc, mid = client.unsubscribe(userdata['topic_command'])
    logging.debug('Unsubscribing from: topic_command' + ' returned rc=' + str(rc) + " mid=" + str(mid))
    # sqlquery
    rc, mid = client.unsubscribe(userdata['topic_sql_query'])
    logging.debug('Unsubscribing from: topic_sql_query' + ' returned rc=' + str(rc) + " mid=" + str(mid))
    # topics and all other topics
    rc, mid = client.unsubscribe('#')
    logging.debug('Unsubscribing from: topic_sql_query' + ' returned rc=' + str(rc) + " mid=" + str(mid))
    pass


def command_processor(client, q, database_filename, userdata, log_all):
    """Implement command processing here."""
    try:
        # DB stuff
        logging.info('Connecting to database ' + database_filename + ' (create table, insert topics)')
        db, dbc, max_topic_id, max_data_id, topics_in_table = \
            db_connect_create_tables(database_filename, userdata['topics_to_log'])
        logging.debug('Database connected.' + ' Max(topic_id)=' + str(max_topic_id) + ' Max(data_id)=' + str(max_data_id))
        dbc.execute('''SELECT * FROM data ORDER BY data_id;''')
        data_in_table = dbc.fetchall()
        logging.debug('data_in_table:' + str(data_in_table))
        logging.debug('Size of data_in_table' + str(len(data_in_table)))
        logging.debug('topics_in_table: ' + str(topics_in_table))
        topic_names = []
        topic_previous = {}
        for topic in topics_in_table:
            topic_names.append(topic[1])
        logging.debug('topic_names: ' + str(topic_names))
        while True:
            # Sender: q.put((topic,m_decode))
            topic, payload = q.get()
            logging.debug('Topic: ' + topic + ' payload: ' + payload)
            # print('Command processor: Topic: ' + topic + ' payload: ' + payload)
            if userdata['topic_command'].find(topic) >= 0:  # is command topic
                splitted_topic = re.split("/", topic)
                command = splitted_topic[1]
                logging.debug('Command processing: ' + command + ' payload: ' + payload)
                print('Command processing: ' + command + ' payload: ' + payload)
                if command == 'command':
                    logging.info('Received command ' + command + ' Payload: ' + payload)
            elif userdata['topic_sql_query'].find(topic) >= 0:  # is SQLQuery topic
                logging.debug('Received sqlquery ' + topic + ' Payload: ' + payload)
                # print('Received sqlquery ' + topic + ' Payload: ' + payload)
                try:
                    request = json.loads(payload)
                    logging.debug('Request:' + str(request))
                    sql_query = request['sql_query']
                    sql_result_topic = request['sql_result_topic']
                    try:
                        dbc.execute(sql_query)
                        sql_query_result = dbc.fetchall()
                        logging.debug('sql_query_result:' + str(sql_query_result))
                        logging.debug('size of sql_query_result=' + str(len(sql_query_result)))
                        # print('sql_query_result:' + str(sql_query_result))
                        # print('size of sql_query_result=' + str(len(sql_query_result)))
                        client.publish(userdata['topic_sql_status'], 'SQL query successfull.')
                        sql_query_repsonse = {
                            "sql_query": sql_query,
                            "sql_query_result": sql_query_result
                        }
                        logging.info('Publish sql query result: ' + sql_result_topic + ', ' + str(sql_query_repsonse))
                        # print('Publish sql query result: ' + sql_result_topic + ', ' + str(sql_query_repsonse))
                        client.publish(sql_result_topic, json.dumps(sql_query_repsonse), qos=2)
                        client.publish(userdata['topic_sql_status'], 'SQL response published.')
                    except Exception as e:
                        logging.error('Exception caught in SQLQuery while executing query:' + ' ' + payload)
                        logging.error(e)
                        # print('Exception caught in SQLQuery while executing query:' + ' ' + payload)
                        # print(e)
                        client.publish(userdata['topic_sql_status'], 'Exception caught in SQLQuery: ' + payload + ' ' + str(e))
                except Exception as e:
                    logging.error('Exception caught in SQLQuery while parsing json payload:' + ' ' + payload)
                    logging.error(e)
                    # print('Exception caught in SQLQuery while parsing json payload:' + ' ' + payload)
                    # print(e)
                    client.publish(userdata['topic_sql_status'], 'Exception caught in SQLQuery: ' + payload + ' ' + str(e))
                pass
            else:  # is topic for DB logging
                if topic in topic_names:
                    logging.debug('log_all=' + str(log_all))
                    logging.debug('payload=' + str(payload))
                    try:
                        previous = topic_previous[topic]
                        logging.debug('previous=' + str(topic_previous[topic]))
                    except KeyError:
                        logging.debug('Previous value of ' + topic + ' not jet existing')
                        previous = None
                        pass
                    if not previous or payload != previous or log_all:
                        # Insert topic data
                        logging.debug('Log topic: ' + topic + ' to database')
                        max_data_id = db_insert_data(
                            db,
                            dbc,
                            max_data_id,
                            topic,
                            payload,
                            topic_names
                        )
                        pass
                    if topic not in topic_previous:
                        logging.debug('Topic: ' + topic + ' not found, add to in topic_previous')
                        topic_previous.update({topic: payload})
                        pass
                    if topic_previous[topic] != payload:
                        logging.debug('Topic: ' + topic + ' replace value in topic_previous')
                        topic_previous[topic] = payload
                        pass
                else:
                    # Insert topic into table topics, then insert data
                    logging.debug('Topic: ' + topic + ' not found in topics_in_table')
                    if db_check_payload_type(payload):
                        max_topic_id, topics_in_table, topic_names = \
                            db_insert_topic(
                                db,
                                dbc,
                                max_topic_id,
                                topics_in_table,
                                topic_names,
                                topic
                            )
                        max_data_id = db_insert_data(
                            db,
                            dbc,
                            max_data_id,
                            topic, payload,
                            topic_names
                        )
                        if topic not in topic_previous:
                            topic_previous.update({topic: payload})
                            pass
                        else:
                            topic_previous[topic] = payload
                            pass
                        pass
                    else:
                        logging.debug('Topic: ' + topic + ' ignored due to unsupported datatype.')
                        pass
                    pass
                pass
            pass
        pass
    except Exception as e:
        logging.error('command_processor: Exception caught:')
        logging.error(e)
        print('command_processor: Exception caught:')
        print(e)
    finally:
        # logging.info('Drop table data...')
        # dbc.execute('''DROP TABLE data;''')
        # logging.info('Drop table topics and close DB.')
        # dbc.execute('''DROP TABLE topics;''')
        logging.info('Disonnecting from database ' + database_filename)
        db.close()
        q.task_done()
    pass


def periodic_processor(client, ii):
    """Insert periodic actions here."""
    # client.publish(topic_status,ii)
    pass


def db_check_payload_type(payload):
    """Check data type of payload."""
    if payload.lower().find('false') >= 0:
        type_is_ok = True
    elif payload.lower().find('true') >= 0:
        type_is_ok = True
    else:
        try:
            float(payload)
            type_is_ok = True
        except ValueError:
            type_is_ok = False
    return type_is_ok


def db_insert_data(db, dbc, max_data_id, topic, payload, topic_names):
    """Insert received topic data into DB SQLite."""
    try:
        dbc.execute('''SELECT datetime('now');''')
        dt = dbc.fetchall()[0]
        topic_id = topic_names.index(topic)
        if payload.lower().find('false') >= 0:
            record = [(max_data_id + 1, topic_id, dt[0], 0.)]
        elif payload.lower().find('true') >= 0:
            record = [(max_data_id + 1, topic_id, dt[0], 1.)]
        else:
            record = [(max_data_id + 1, topic_id, dt[0], float(payload))]
        logging.debug('Insert record: ' + str(record))
        dbc.executemany('INSERT INTO data VALUES(?,?,?,?);', record)
        db.commit()
        max_data_id += 1
    except sqlite3.IntegrityError as e:
        logging.error('Data record not inserted due to exception.')
        logging.error(e)
        pass
    return max_data_id


def db_insert_topic(db, dbc, max_topic_id,
                    topics_in_table,
                    topic_names,
                    new_topic
                    ):
    """Insert topic data into DB SQLite."""
    new_topics = []
    if new_topic not in topics_in_table:
        logging.debug(new_topic + ' is not existing.')
        new_topics.append((max_topic_id + 1, new_topic))
        topic_names.append(new_topic)
        max_topic_id += 1
    else:
        logging.debug(new_topic + ' is already existing.')
        pass
    logging.debug('new_topics: ' + str(new_topics))
    try:
        dbc.executemany('INSERT INTO topics VALUES(?,?);', new_topics)
        db.commit()
    except sqlite3.IntegrityError as e:
        logging.error('Data record not inserted due to exception.')
        logging.error(str(new_topics))
        logging.error(str(e))
        pass
    dbc.execute('''SELECT * FROM topics ORDER BY topic_id;''')
    topics_in_table = dbc.fetchall()
    logging.info('Size of topics_in_table=' + str(len(topics_in_table)))
    logging.info('Max(topic_id)=' + str(max_topic_id))
    logging.info('topics_in_table:' + str(topics_in_table))
    return max_topic_id, topics_in_table, topic_names


def db_connect_create_tables(database_filename, topics):
    """Connect DB SQLite and create tables."""
    logging.debug('DB: ' + database_filename)
    # Open DB and crreate cusor
    db = sqlite3.connect(database_filename)
    dbc = db.cursor()
    # TABLE TOPICS
    # dbc.execute('''DROP TABLE IF EXISTS topics;''')
    logging.debug('Create table topics...')
    dbc.execute('''CREATE TABLE IF NOT EXISTS topics (topic_id integer PRIMARY KEY, topic_name text NOT NULL UNIQUE)''')
    db.commit()
    # Query table topics
    dbc.execute('''SELECT * FROM TOPICS;''')
    rows = dbc.fetchall()
    logging.debug('Size of topics=' + str(len(rows)) + ' Rows=' + str(rows))
    if len(rows) != 0:
        dbc.execute('''SELECT MAX(topic_id) FROM topics;''')
        max_topic_id = dbc.fetchall()[0][0]
    else:
        max_topic_id = 0
    logging.debug('Max(topic_id)=' + str(max_topic_id))
    topics_in_table = []
    for row in rows:
        topics_in_table.append(row[1])
    logging.debug('topics_in_table: ' + str(topics_in_table))
    # Add topics
    logging.debug('insert topics into table topics...')
    logging.debug(str(topics))
    new_topics = []
    for topic in topics:
        if topic.find('+') >= 0 or topic.find('#') >= 0:
            # ignote topic with wildcard
            logging.info(topic + ' ignored due to wildcard.')
            pass
        else:
            if topic not in topics_in_table:
                logging.debug(topic + ' is not existing, insert into DB.')
                new_topics.append((max_topic_id, topic))
                max_topic_id += 1
                pass
            pass
    logging.debug('new_topics: ' + str(new_topics))
    try:
        dbc.executemany('INSERT INTO topics VALUES(?,?);', new_topics)
        db.commit()
    except sqlite3.IntegrityError as e:
        logging.error('Data record not inserted due to exception.')
        logging.error(str(e))
        pass
    dbc.execute('''SELECT * FROM topics ORDER BY topic_id;''')
    topics_in_table = dbc.fetchall()
    logging.debug('Size of topics_in_table=' + str(len(topics_in_table)))
    logging.debug('Max(topic_id)=' + str(max_topic_id))
    logging.debug('topics_in_table:' + str(topics_in_table))
    # TABLE DATA
    # dbc.execute('''DROP TABLE IF EXISTS data;''')
    logging.debug('Create table data...')
    dbc.execute('''CREATE TABLE IF NOT EXISTS data (data_id integer PRIMARY KEY, topic_id integer NOT NULL, datetime text NOT NULL, data real NOT NULL, FOREIGN KEY (topic_id) REFERENCES topics (topic_id))''')
    db.commit()
    # Query table data
    dbc.execute('''SELECT * FROM data;''')
    rows = dbc.fetchall()
    logging.debug('data: Size of rows:' + str(len(rows)) + ' Rows:' + str(rows))
    if len(rows) != 0:
        dbc.execute('''SELECT MAX(data_id) FROM data;''')
        max_data_id = dbc.fetchall()[0][0]
    else:
        max_data_id = 0
    logging.debug('Max(data_id)=' + str(max_data_id))
    # logging.warning('Drop table data...')
    # dbc.execute('''DROP TABLE data;''')
    return db, dbc, max_topic_id, max_data_id, topics_in_table


def run_logger(broker='localhost', port=1883, client_id='mqtt-sqlite-logger', database_fname='history.sqlite', topics_fname='topics.json', log_all=False, user=None, pwd=None):
    """
    This function can be used to log MQTT-Topics to SQLite database.

    Python API entry point for logger.

    Parameters
    ----------
    broker : string, optional
        MQTT broker node. The default is 'localhost'.
    port : int, optional
        MQTT broker port. The default is 1883.
    client_id : sring, optional
        MQTT client_id. The default is 'mqtt-sqlite-logger'.
    database_fname : string (path), optional
        Filename for the SQLiteDB. The default is 'history.sqlite'.
    topics_fname : string (path), optional
        Filename containing the MQTT topics to monitor. The default is 'topics.json'.
    log_all : bool, optional
        Flag to indicate if all topic update become stored in database or change values only. The default is False.

    Returns
    -------
    None.

    """
    logging.info('Broker: ' + broker)
    logging.info('Port: ' + str(port))
    logging.info('Client_id: ' + client_id)
    # How to make following variables available to MQTT callbacks
    topic_command = client_id + '/command'
    topic_sql_query = client_id + '/sql_query'
    topic_sql_status = client_id + '/sql_status'
    topic_status = client_id + '/status'
    topic_version = client_id + '/version'
    database_filename = database_fname
    topics_filename = topics_fname
    logging.info('topic_command: ' + topic_command)
    logging.info('topic_sql_query: ' + topic_sql_query)
    logging.info('topic_sql_status: ' + topic_sql_status)
    logging.info('topic_status: ' + topic_status)
    logging.info('topic_version: ' + topic_version)
    logging.info('database_filename: ' + database_filename)
    logging.info('topics_filename: ' + topics_filename)
    # client = mqtt.Client(client_id='', clean_session=True, userdata=None, protocol=MQTTv311, transport='tcp')
    try:
        q = queue.Queue()
        with open(topics_filename,) as fObj:
            topics_to_log = []
            topics = json.load(fObj)['topics']
            logging.debug('Topics:' + str(topics))
            for topic in topics:
                topics_to_log.append(topic)
            # print('topics_to_log:', topics_to_log)
        userdata = {'q': q,
                    'client_id': client_id,
                    'topic_command': topic_command,
                    'topic_sql_query': topic_sql_query,
                    'topic_sql_status': topic_sql_status,
                    'topic_status': topic_status,
                    'topic_version': topic_version,
                    'topics_to_log': tuple(topics_to_log)
                    }
        client = mqtt.Client(client_id, clean_session=True)
        client.connected_flag = False
        client.will_set(client_id, 'Offline', 1, False)
        client.user_data_set(userdata)
        if user is not None and pwd is not None:
            client.username_pw_set(username=user, password=pwd)
        # bind call back function
        client.on_connect = on_connect
        client.on_disconnect = on_disconnect
        # client.on_log = on_log
        client.on_publish = on_publish
        client.on_message = on_message
        client.on_subscribe = on_subscribe
        client.on_unsubscribe = on_unsubscribe
        # turn-on the worker thread
        logging.info('Start thread command_processor')
        threading.Thread(target=command_processor, args=(client, q, database_filename, userdata, log_all, ), daemon=True).start()
        logging.info('Starting client loop and connect ' + client_id + ' to broker ' + broker)
        client.loop_start()
        client.connect(broker, port=1883, keepalive=60, bind_address='')
        while not client.connected_flag:
            print('Waiting for', broker, '...')
            time.sleep(1)
        time.sleep(3)
        print('Waiting for exception (Ctrl+c) ...')
        ii = 0
        while client.connected_flag:
            ii += 1
            periodic_processor(client, ii)
            time.sleep(1)
    except BaseException as e:
        logging.error('main: Exception cought!' + str(e))
        client.connected_flag = False
    finally:
        unsubscribe_all(client, userdata)
        q.join()
        logging.debug('Publishing: disconnected')
        rc, mid = client.publish(client_id, 'disconnected')
        logging.debug('Publishing: disconnected returned rc=' + str(rc) + ' mid=' + str(mid))
        logging.info('Disonnecting from broker ' + broker)
        client.disconnect()
        time.sleep(1)
        logging.info('Stopping message loop')
        client.loop_stop(force=False)
    pass


def cli():
    """Command-line interface for mqtt-sqlite-logger."""
    # Logging setup
    user_name = os.getlogin()
    app_name = re.split('.py', os.path.basename(__file__))[0]
    dirs = PlatformDirs(app_name, user_name)
    default_db_filename = os.path.join(dirs.user_data_dir, 'history.sqlite')
    default_log_filename = os.path.join(dirs.user_log_dir, 'logger.log')
    print('Default database filename:', default_db_filename)
    print('Default log filename:', default_log_filename)
    parser = ArgumentParser(
        description='The programm can be used to log MQTT-Topics to SQLite database.',
        epilog='Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH',
        prog='py logger.py'
    )
    parser.add_argument('-a', '--all', dest='log_all', default=False)
    parser.add_argument('-b', '--broker', dest='broker', default='localhost')
    parser.add_argument('-p', '--port', dest='port', default=1883)
    parser.add_argument('-c', '--client_id', dest='client_id', default='mqtt-sqlite-logger')
    parser.add_argument('--debug', dest='debug_level', default=logging.WARNING)
    parser.add_argument('-d', '--database', dest='database_filename', default=default_db_filename)
    parser.add_argument('-l', '--log', dest='log_filename', default=default_log_filename)
    parser.add_argument('-t', '--topics', dest='topics_filename', default='topics.json')
    args = parser.parse_args()
    broker = args.broker
    port = int(args.port)
    client_id = args.client_id
    log_all = args.log_all
    topics_filename = args.topics_filename
    database_filename = args.database_filename
    if not os.path.exists(os.path.dirname(database_filename)):
        os.makedirs(os.path.dirname(database_filename), exist_ok=True)
    print('Database filename:', database_filename)
    log_filename = args.log_filename
    if not os.path.exists(os.path.dirname(log_filename)):
        os.makedirs(os.path.dirname(log_filename), exist_ok=True)
    print('Log filename:', default_log_filename)
    logging.basicConfig(
        filename=log_filename, filemode='w+', encoding='utf-8',
        level=args.debug_level,
        style='{',
        format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}'
    )
    time.sleep(1)
    if broker == 'localhost':
        tmp = input('MQTT Broker (' + broker + ')? ')
        if tmp != '':
            broker = tmp
    user = input('MQTT username: ')
    if len(user) == 0:
        user = None
    pwd = getpass.getpass()
    if len(pwd) == 0:
        pwd = None
    logging.info('Calling run_logger() from cli()')
    run_logger(broker, port, client_id, database_filename, topics_filename, log_all, user, pwd)
    logging.info('Script execution stopped.')
    logging.shutdown()


if __name__ == '__main__':
    print(os.path.basename(__file__), '\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n\
Published under EUPL.\n\
        ')
    cli()
    print('Script execution stopped.')
    sys.exit()
