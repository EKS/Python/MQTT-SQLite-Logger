MQTT-SQLite-Logger
==================
The programm can be used to log MQTT-Topics to SQLite database.
SQLite database becomes created if not existing.
It can also be used to query the SQLite database via MQTT.

Run logger from command line:
----------------------
- `py logger.py -h`
- `py logger.py -b localhost -d history.sqlite -l logger.log -t topics.json`
- `mqtt-sqlite-logger -b localhost -d history.sqlite -l logger.log -t topics.json`

Define topics to log in JSON format, Wildcards (+,#) are suported, example:

`
{
    "topics": [
        "mqtt-sqlite-generator/double",
        "mqtt-sqlite-generator/integer"
    ]
}
`

Query SQLite DB
---------------
There are three ways to query the SQLite DB. The logger must be active!

### 1 Interactive
Query the SQLite database via MQTT Explorer by writing to a topic, example:
- Topic: mqtt-sqlite-logger/sqlquery
- Payload:

`
{ "sql_query": "select datetime, data from data where datetime>'2023-01-20 17:30:00' AND topic_id=(select topic_id from topics WHERE topic_name = 'mqtt-sqlite-generator/integer') ORDER BY datetime ASC",
  "sql_result_topic": "sql_query_result"}
`

### 2 Command line
- `py query.py -h`
- mqtt-sqlite-query -h
Can be used to send a query, defined in a file (default: 'topic.json'), and wait for the query results, which become displayed in a matplotlib plot.

### 3 Jupyter
query.ipynb can be used to send a query and wait for the query results, which become displayed in a matplotlib plot.

Generate random value topics
----------------------------
- `py generator.py -h`
- mqtt-sqlite-generator -h
Can be used to generate random data in topics integer and double.

Dependencies
------------
### Mandatory
- *paho-mqtt*: https://pypi.org/project/paho-mqtt/

### Optional
- *matplotlib*: https://pypi.org/project/matplotlib/ (used in MQTT-SQLite-Query.py)
- *jupyterlab*: https://pypi.org/project/jupyterlab/

Known issues
------------
- Supported data types, all converted to float:
  - boolean {false/true}
  - integer and 
  - float, only.
- SQL query results may be plotted wehn call from command line, but in python environment only.

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Licensed under the EUPL. Refer to license files on disc.

Acknowledgements
----------------
- The MQTT part is inpired by http://www.steves-internet-guide.com
- SQLite part is inpired from https://pythonexamples.org/python-sqlite3-tutorial
- Special thanks to Dennis Klein (D.Klein@gsi.de). He supported the package building.

Revision History
----------------
Revision 1.2.2.1 - 08.02.2023 H.Brand@gsi.de
- Add to Acknowledgements.

Revision 1.2.2.0 - 08.02.2023 H.Brand@gsi.de
- Use platformdirs as default path
  - Log logger.log to user_log_dir
  - Save database history.sqlite to user_data_dir by default
  - Tested on linux only.

Revision 1.2.1.16 - 30.01.2023 H.Brand@gsi.de
- Add username and password.

Revision 1.2.1.14 - 26.01.2023 H.Brand@gsi.de
- Handle matplotlib exception in query.py.
- Update README.md.

Revision 1.2.1.13 - 25.01.2023 H.Brand@gsi.de
- Fixed runtime errors.

Revision 1.2.0.0 - 13.01.2023 H.Brand@gsi.de
- Refactoring in order to build a package for https://pypi.org/

Revision 1.1.1.0 - 05.05.2022 H.Brand@gsi.de
- Modify code with respect to coding recommendations by spyder
- Combine sending a query and receiving the results into one script.
- Add the broker to the command-line in bat-file.

Revision 1.1.0.0 - 08.04.2022 H.Brand@gsi.de
- Log all or changed values only to SQLite
Revision 1.0.0.1 - 25.03.2022 H.Brand@gsi.de
- Logging to SQLite
- Query SQLite (Topic: SQLQuery)
- Prepared to handle commands (Topic: Command)
